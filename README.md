# CKAN Data Viewer

Static mock-up designs for the CKAN Data Viewer component (Toronto RFP).

## Install

Run `npm install` to install all required `npm` packages.

## Build

Run `npx gulp` to build all front-end assets and start a server with Browser Sync.
