var ctx = document.getElementById('pointChart').getContext('2d');

var pointChart = new Chart(ctx, {
  type: 'scatter',
  data: pointChartData,
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    legend: {
      position: 'bottom'
    }
  }
});

Sortable.create(document.getElementById('all-columns'), {
  group: {
    name: 'columns',
    pull: 'clone',
    put: ['x-axis', 'y-axis', 'colour-attr']
  },
  animation: 150
});

Sortable.create(document.getElementById('x-axis'), {
  group: {
    name: 'x-axis',
    put: ['columns']
  },
  animation: 150,
  onAdd: function onAdd(evt) {
    onColumnAdd(evt);
  },
  onRemove: function onRemove(evt) {
    onColumnRemove(evt);
  }
});

Sortable.create(document.getElementById('y-axis'), {
  group: {
    name: 'y-axis',
    put: ['columns']
  },
  animation: 150,
  onAdd: function onAdd(evt) {
    onColumnAdd(evt);
  },
  onRemove: function onRemove(evt) {
    onColumnRemove(evt);
  }
});

Sortable.create(document.getElementById('colour-attr'), {
  group: {
    name: 'colour-attr',
    put: ['columns']
  },
  animation: 150
});

function onColumnAdd(evt) {
  var item = $(evt.item);
  var column = item.attr('data-column');
  var to = $(evt.to).attr('id');
  if (columns[column]) {
    if (to === 'x-axis') {
      if (pointChartData.datasets[0].data.length === 0) {
        pointChartData.datasets[0].data = columns[column].map(function(num) {
          return {
            x: num
          };
        });
      } else {
        pointChartData.datasets[0].data = columns[column].map(function(num, i) {
          return {
            ...pointChartData.datasets[0].data[i],
            x: num
          };
        });
      }
    } else if (to === 'y-axis') {
      if (pointChartData.datasets[0].data.length === 0) {
        pointChartData.datasets[0].data = columns[column].map(function(num) {
          return {
            y: num
          };
        });
      } else {
        pointChartData.datasets[0].data = columns[column].map(function(num, i) {
          return {
            ...pointChartData.datasets[0].data[i],
            y: num
          };
        });
      }
    }
    pointChart.update();
  }
}

function onColumnRemove(evt) {
  var item = $(evt.item);
  var column = item.attr('data-column');
  var from = $(evt.from).attr('id');
  if (columns[column]) {
    if (from === 'x-axis') {
      pointChartData.datasets[0].data = pointChartData.datasets[0].data.map(
        function(point, i) {
          delete point.x;
          return point;
        }
      );
    } else if (from === 'y-axis') {
      pointChartData.datasets[0].data = pointChartData.datasets[0].data.map(
        function(point, i) {
          delete point.y;
          return point;
        }
      );
    }
    item.remove();
    pointChart.update();
  }
}
