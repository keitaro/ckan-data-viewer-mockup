var barChartData = {
  labels: [],
  datasets: [
    {
      label: 'Some dataset label',
      data: [],
      backgroundColor: [
        '#332288',
        '#117733',
        '#44AA99',
        '#88CCEE',
        '#DDCC77',
        '#CC6677',
        '#AA4499',
        '#882255'
      ]
    }
  ]
};

var lineChartData = {
  labels: [],
  datasets: [
    {
      label: 'Some dataset label',
      data: [],
      backgroundColor: ['#332288']
    }
  ]
};

var pointChartData = {
  labels: [],
  datasets: [
    {
      label: 'Some dataset label',
      data: [],
      backgroundColor: [
        '#332288',
        '#117733',
        '#88ccee',
        '#ddcc77',
        '#aa4499',
        '#882255'
      ]
    }
  ]
};

var columns = {
  temp: [12, 16, 8, 18, 15, 3, 9],
  days: [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
  ],
  costPerClick: [5, 2, 4, 8, 7, 6, 5, 2, 3, 3, 3, 6],
  numOfTransactions: [4, 14, 12, 12, 11, 20, 18, 10, 4, 5, 8, 12]
};
